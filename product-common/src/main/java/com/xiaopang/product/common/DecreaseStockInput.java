package com.xiaopang.product.common;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 购物车传入参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DecreaseStockInput {
    /**
     * 商品Id
     */
    private String productId;
    /**
     * 商品数量
     */
    private Integer productQuantity;
}
