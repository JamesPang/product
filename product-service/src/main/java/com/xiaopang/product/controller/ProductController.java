package com.xiaopang.product.controller;

import com.xiaopang.product.common.DecreaseStockInput;
import com.xiaopang.product.common.ProductInfoOutput;
import com.xiaopang.product.dataobject.ProductCategory;
import com.xiaopang.product.dataobject.ProductInfo;
import com.xiaopang.product.service.ProductCategoryService;
import com.xiaopang.product.service.ProductInfoService;
import com.xiaopang.product.utils.ResultVOUtil;
import com.xiaopang.product.vo.ProductInfoVO;
import com.xiaopang.product.vo.ProductVO;
import com.xiaopang.product.vo.ResultVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductInfoService productInfoService;

    @Autowired
    private ProductCategoryService productCategoryService;

    /**
     * 1.查询所有在架商品
     * 2.查询类目type列表
     * 3.查询类目
     * 4.构造互数据
     */
    @GetMapping("/list")
    public ResultVO<ProductVO> list(){
        //1.查询所有在架商品
        List<ProductInfo> productInfoList = productInfoService.findUp();
        //2.查询类目type列表
        List<Integer> categoryTypeList = productInfoList.stream()
                        .map(ProductInfo::getCategoryType)
                        .collect(Collectors.toList());
        //3.查询类目
        List<ProductCategory> productCategoryList =
                productCategoryService.findByCategoryTypeIn(categoryTypeList);
        //4.构造数据
        List<ProductVO> productVOList = new ArrayList<>();
        for (ProductCategory productCategory : productCategoryList) {

            ProductVO productVo = new ProductVO();
            productVo.setCategoryName(productCategory.getCategoryName());
            productVo.setCategoryType(productCategory.getCategoryType());
            List<ProductInfoVO> productInfoVoList = new ArrayList<>();
            for (ProductInfo productInfo:productInfoList) {
                if(productCategory.getCategoryType().equals(productInfo.getCategoryType())){
                    ProductInfoVO productInfoVo = new ProductInfoVO();
                    BeanUtils.copyProperties(productInfo,productInfoVo);
                    productInfoVoList.add(productInfoVo);
                }
            }
            productVo.setProductInfoVoList(productInfoVoList);
            productVOList.add(productVo);
        }
        return ResultVOUtil.success(productVOList);
    }

    /**
     * 获取商品列表(提供给订单服务)
     * @param productIdList
     * @return
     */
    @PostMapping("/listForOrder")
    public List<ProductInfoOutput> listForOrder(@RequestBody List<String> productIdList){
        return productInfoService.listForOrder(productIdList);
    }

    /**
     * 减库存
     * @param decreaseStockInputList
     */
    @PostMapping("/decreateStock")
    public void decreateStock(@RequestBody List<DecreaseStockInput> decreaseStockInputList){
        productInfoService.decreateStock(decreaseStockInputList);
    }
}
