package com.xiaopang.product.service;

import com.xiaopang.product.common.DecreaseStockInput;
import com.xiaopang.product.common.ProductInfoOutput;
import com.xiaopang.product.dataobject.ProductInfo;

import java.util.List;

/**
 * 商品service
 */
public interface ProductInfoService {
    List<ProductInfo> findUp();

    List<ProductInfoOutput> listForOrder(List<String> productIdList);

    void decreateStock(List<DecreaseStockInput> cartDTOList);
}
