package com.xiaopang.product.service;

import com.xiaopang.product.dataobject.ProductCategory;

import java.util.List;

/**
 * 类目
 */
public interface ProductCategoryService {

    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

}
