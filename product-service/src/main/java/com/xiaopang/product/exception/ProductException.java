package com.xiaopang.product.exception;

import com.xiaopang.product.enums.ResultEnum;
import lombok.Data;

@Data
public class ProductException extends RuntimeException {
    private Integer code;

    public ProductException(Integer code,String message){
        super(message);
        this.code = code;
    }

    public ProductException(ResultEnum resultEnum){
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }
}
