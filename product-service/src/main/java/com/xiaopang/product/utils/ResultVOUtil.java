package com.xiaopang.product.utils;

import com.xiaopang.product.vo.ResultVO;

/**
 * 返回值结果工具类
 */
public class ResultVOUtil {
    public static ResultVO success(Object object){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(0);
        resultVO.setMsg("查询成功");
        resultVO.setData(object);
        return resultVO;
    }
}
