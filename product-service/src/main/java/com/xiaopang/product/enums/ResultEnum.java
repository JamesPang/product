package com.xiaopang.product.enums;

import lombok.Getter;

/**
 * 返回结果枚举
 */
@Getter
public enum  ResultEnum {
    PRODUCT_NOT_EXISTS(1,"商品不存在"),
    PRODUCT_STOCT_ERROR(2,"商品库存错误"),;

    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
