package com.xiaopang.product.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;


@Data
public class ProductInfoVO {
    /**
     * 产品id
     */
    @JsonProperty("id")
    private String productId;
    /**
     * 产品名称
     */
    @JsonProperty("name")
    private Integer productName;

    /** 商品单价 */
    @JsonProperty("price")
    private BigDecimal productPrice;

    /** 描述 */
    @JsonProperty("description")
    private String productDescription;

    /**
     * 图片
     */
    @JsonProperty("icon")
    private String productIcon;


}
