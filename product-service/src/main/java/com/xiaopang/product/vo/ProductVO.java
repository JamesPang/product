package com.xiaopang.product.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@Data
public class ProductVO {
    /**
     * 类目名字
     */
    @JsonProperty("name")
    private String categoryName;
    /**
     * 类目编号
     */
    @JsonProperty("type")
    private Integer categoryType;

    /**
     * 产品信息
     */
    @JsonProperty("foods")
    private List<ProductInfoVO> productInfoVoList;
}
