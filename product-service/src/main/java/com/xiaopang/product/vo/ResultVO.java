package com.xiaopang.product.vo;

import lombok.Data;

/**
 * http返回的最外层对象
 */
@Data
public class ResultVO<T> {
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 返回消息
     */
    private String msg;
    /**
     * 返回具体内容
     */
    private T data;
}
