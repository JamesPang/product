package com.xiaopang.product.client;

import com.xiaopang.product.common.DecreaseStockInput;
import com.xiaopang.product.common.ProductInfoOutput;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name="product")
@RequestMapping("/product")
public interface ProductClient {
    /**
     * 获取商品列表(提供给订单服务)
     * @param productIdList
     * @return
     */
    @PostMapping("/listForOrder")
    List<ProductInfoOutput> listForOrder(@RequestBody List<String> productIdList);

    @PostMapping("/decreateStock")
    void decreateStock(@RequestBody List<DecreaseStockInput> cartDTOList);
}
